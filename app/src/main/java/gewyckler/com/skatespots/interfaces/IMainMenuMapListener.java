package gewyckler.com.skatespots.interfaces;

import gewyckler.com.skatespots.model.SkateSpot;

public interface IMainMenuMapListener {

    void initSkateSpotDetailsWindow(SkateSpot skateSpot);

    void removeCurrentSkateSpotDetailsWindow();

    void removeSkateSpotFromList(Long id);

}
