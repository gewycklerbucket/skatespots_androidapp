package gewyckler.com.skatespots.service;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import gewyckler.com.skatespots.model.SkateSpot;
import gewyckler.com.skatespots.repository.JsonPlaceHolderApi;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SkatePointApiService {
    private static final String TAG = "SkatePointApiService";
    private static final String BASE_URL = "https://skate-point.herokuapp.com/";
    private static SkatePointApiService skatePointApiService = null;

    private List<SkateSpot> skateSpotList = new ArrayList<>();
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    private Retrofit retrofit;

    private SkatePointApiService() {
    }

    public static SkatePointApiService getInstance() {
        if (skatePointApiService == null) {
            skatePointApiService = new SkatePointApiService();
        }
        return skatePointApiService;
    }

    private Retrofit getRetrofit() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient).build();
        }
        return retrofit;
    }

    public List<SkateSpot> getAllSkateSpotRequest() {
        Log.d(TAG, "getAllSkateSpotRequest: called");
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<SkateSpot>> call = jsonPlaceHolderApi.getSkateSpots();

        call.enqueue(new Callback<List<SkateSpot>>() {
            @Override
            public void onResponse(@NonNull Call<List<SkateSpot>> call, @NonNull Response<List<SkateSpot>> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "onResponse: getAllSkateSpotRequest response is not successful: " + response.message());
                    return;
                }
                if (response.body().isEmpty()) {
                    Log.d(TAG, "onResponse: getAllSkateSpotRequest response body is empty: " + response.message());
                    return;
                }
                skateSpotList = response.body();
            }

            @Override
            public void onFailure(@NonNull Call<List<SkateSpot>> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: getAllSkateSpotRequest response failed: " + t.getMessage());
            }
        });
        return skateSpotList;
    }

    public List<SkateSpot> getParkSpotsRequest() {
        Retrofit retrofit = getRetrofit();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<SkateSpot>> call = jsonPlaceHolderApi.getSkateSpotByType("SKATEPARK");
        call.enqueue(new Callback<List<SkateSpot>>() {
            @Override
            public void onResponse(@NonNull Call<List<SkateSpot>> call, @NonNull Response<List<SkateSpot>> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "onResponse: getParkSpotsRequest response is not successful: " + response.message());
                    return;
                }
                if (response.body().isEmpty()) {
                    Log.d(TAG, "onResponse: getParkSpotsRequest response body is empty: " + response.message());
                    return;
                }
                skateSpotList = response.body();
            }

            @Override
            public void onFailure(@NonNull Call<List<SkateSpot>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: getParkSpotsRequest response failed: " + t.getMessage());
            }
        });
        return skateSpotList;
    }

    public List<SkateSpot> getStreetSpotsRequest() {
        Retrofit retrofit = getRetrofit();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<SkateSpot>> call = jsonPlaceHolderApi.getSkateSpotByType("STREET_SPOT");
        call.enqueue(new Callback<List<SkateSpot>>() {
            @Override
            public void onResponse(@NonNull Call<List<SkateSpot>> call, @NonNull Response<List<SkateSpot>> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "onResponse: getStreetSpotsRequest response is not successful: " + response.message());
                    return;
                }
                if (response.body().isEmpty()) {
                    Log.d(TAG, "onResponse: getStreetSpotsRequest response body is empty: " + response.message());
                    return;
                }
                skateSpotList = response.body();
            }

            @Override
            public void onFailure(@NonNull Call<List<SkateSpot>> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: getStreetSpotsRequest response failed: " + t.getMessage());
            }
        });
        return skateSpotList;
    }

    /**
     * Create SkateSpot object with and send it to database
     *
     * @param skateSpot contains data related to place that it describe
     */
    public void createSkateSpotRequest(SkateSpot skateSpot) {

        Retrofit retrofit = getRetrofit();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<SkateSpot> call = jsonPlaceHolderApi.createSkateSpot(skateSpot);
        call.enqueue(new Callback<SkateSpot>() {
            @Override
            public void onResponse(@NonNull Call<SkateSpot> call, @NonNull Response<SkateSpot> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "onResponse: createSkateSpotRequest response is NOT successful: " +
                            response.message() + "__\n" + response.code() + "__\n" + response.toString());
                    return;
                }

                Log.d(TAG, "onResponse: createSkateSpotRequest response is successful: " +
                        response.message() + response.code());
            }

            @Override
            public void onFailure(@NonNull Call<SkateSpot> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: createSkateSpotRequest response failed: " + t.getMessage());
            }
        });
    }

    public void deleteSkateSpot(Long id) {
        Predicate<SkateSpot> isQualified = item -> item.getSkateSpotId().equals(id);

        Retrofit retrofit = getRetrofit();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<Long> call = jsonPlaceHolderApi.deleteSkateSpot(id);
        call.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(@NonNull Call<Long> call, @NonNull Response<Long> response) {
                Log.e(TAG, "onResponse: deleteSkateSpot response: " + response.code());
                skateSpotList.removeIf(isQualified);
            }

            @Override
            public void onFailure(@NonNull Call<Long> call, @NonNull Throwable t) {
                Log.e(TAG, "onResponse: deleteSkateSpot response failed: " + t.getMessage());
            }
        });
    }
}
