package gewyckler.com.skatespots.utilities;

import com.google.android.gms.maps.model.LatLng;

public class Constants {
    public static final int REQUEST_CODE_DELETE_SKATESPOT = 9002;

    public static final int REQUEST_CODE_FINE_LOCATION_PERMISSION = 8001;
    public static final int REQUEST_CODE_INTERNET_PERMISSION = 8002;

    public static final int ERROR_DIALOG_REQUEST = 6001;

    public static final String SKATESPOT_DETAILS = "SKATESPOT_DETAILS";
    public static final String DELETED_SKATESPOT_ID = "DELETED_SKATESPOT_ID";

    public static final String SKATESPOT_DEFAULT_NAME = "Default Name";

    public static final String SKATESPOT_FRAGMENT_TAG = "SKATESPOT_FRAGMENT_TAG";
    public static final String MAP_FRAGMENT_TAG = "MAP_FRAGMENT_TAG";
    public static final String SKATESPOT_DETAILS_WINDOW_FRAGMENT_TAG = "SKATESPOT_DETAILS_WINDOW_FRAGMENT_TAG";
    public static final String LOCALIZATION_BY_MARKER_TAG = "LOCALIZATION_BY_MARKER_TAG";

    public static boolean INTERNET_PERMISSION_GRANTED = false;

    public static final int CLUSTER_MARGIN = 100;
    public static final float USER_POSITION_ZOOM = 15F;
    public static final float DEFAULT_ZOOM = 5.6f;

    public static final LatLng CENTER_OF_POLAND = new LatLng(52.065162, 19.252522);
}
