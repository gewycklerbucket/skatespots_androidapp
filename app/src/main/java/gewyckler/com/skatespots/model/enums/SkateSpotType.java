package gewyckler.com.skatespots.model.enums;

public enum SkateSpotType {
    //    SKATEPARK("SKATE PARK", ),
//    STREET_SPOT("STREET SPOT", R.drawable.);
    SKATEPARK,
    STREET_SPOT;
//    private String type;
//    private Integer iconId;

//    SkateSpotType(String type, Integer iconId) {
//        this.type = type;
//        this.iconId = iconId;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public Integer getIconId() {
//        return iconId;
//    }
}
