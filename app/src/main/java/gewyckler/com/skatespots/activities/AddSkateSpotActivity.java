package gewyckler.com.skatespots.activities;

import static gewyckler.com.skatespots.utilities.Constants.LOCALIZATION_BY_MARKER_TAG;
import static gewyckler.com.skatespots.utilities.Constants.SKATESPOT_FRAGMENT_TAG;

import android.location.Address;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.fragments.LocalizationByMarkerFragment;
import gewyckler.com.skatespots.fragments.SkateSpotSettingFragment;
import gewyckler.com.skatespots.interfaces.ILocalizationByMarkerListener;

public class AddSkateSpotActivity extends AppCompatActivity implements
        ILocalizationByMarkerListener {

    private static final String TAG = "AddSkateSpotActivity";

    private SkateSpotSettingFragment skateSpotSettingFragment;
    private LocalizationByMarkerFragment localizationByMarkerFragment;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_skatespot_layout);
        setTitle(getResources().getString(R.string.create_skatespot));

        if (savedInstanceState != null) {
            skateSpotSettingFragment = (SkateSpotSettingFragment) getSupportFragmentManager().findFragmentByTag(SKATESPOT_FRAGMENT_TAG);
            localizationByMarkerFragment = (LocalizationByMarkerFragment) getSupportFragmentManager().findFragmentByTag(LOCALIZATION_BY_MARKER_TAG);
        } else {
            skateSpotSettingFragment = new SkateSpotSettingFragment();
            loadFragment(R.id.add_skatespot_frame_layout, skateSpotSettingFragment, SKATESPOT_FRAGMENT_TAG);
        }
        viewInit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_skatespot_top_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_photo:
                skateSpotSettingFragment.selectImage();
                return false;

//            case R.id.set_localization_by_marker:
//                localizationByMarkerFragment = new LocalizationByMarkerFragment();
//                loadFragment(R.id.add_skatespot_frame_layout, localizationByMarkerFragment, LOCALIZATION_BY_MARKER_TAG);
//                return false;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void viewInit() {
        Log.d(TAG, "viewInit: called.");

        Toolbar toolbar = findViewById(R.id.skatespot_top_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigation_bar);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_general:
                        loadFragment(R.id.add_skatespot_frame_layout, skateSpotSettingFragment, SKATESPOT_FRAGMENT_TAG);
                        break;

                    case R.id.nav_localization:
                        localizationByMarkerFragment = new LocalizationByMarkerFragment();
                        loadFragment(R.id.add_skatespot_frame_layout, localizationByMarkerFragment, LOCALIZATION_BY_MARKER_TAG);
                        break;
                }
                return true;
            }
        });
        checkForActiveFragment();
    }

    @Override
    public void sentAddressList(List<Address> addressList) {
        skateSpotSettingFragment.setLocalizationInViewModel(addressList);
    }

    private void checkForActiveFragment() {
        if (localizationByMarkerFragment != null) {
            loadFragment(R.id.add_skatespot_frame_layout, localizationByMarkerFragment, LOCALIZATION_BY_MARKER_TAG);
        }
    }

    private void loadFragment(@IdRes int layoutId, Fragment fragment, String TAG) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(layoutId, fragment, TAG)
                .addToBackStack(TAG)
                .commit();
    }
}