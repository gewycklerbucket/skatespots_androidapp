package gewyckler.com.skatespots.utilities;

import static gewyckler.com.skatespots.utilities.Constants.REQUEST_CODE_FINE_LOCATION_PERMISSION;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import gewyckler.com.skatespots.R;

public class Alerts {
    private static final String TAG = "Alerts";
    private static AlertDialog alertDialog;

    public static void permissionNotGrantedAlert(/*String message,*/ Context context) {
        Log.d(TAG, "permissionNotGrantedAlert: called");
        alertDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.permission_not_granted)
                .setMessage(R.string.permission_necessary_info)
                .setPositiveButton(R.string.yes_info, (dialog, which) -> {
                    Intent i = new Intent();
                    i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.setData(Uri.parse("package:" + context.getPackageName()));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    context.startActivity(i);
                })
                .setNeutralButton(R.string.no_info, (dialog, which) -> {
                    Toast.makeText(context, R.string.permission_not_granted, Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                })
                .show();
    }

    public static void gpsIsNotEnableAlert(Context context, Activity activity) {
        Log.d(TAG, "gpsIsNotEnableAlert: called");
        alertDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.gps_disable_info)
                .setMessage(R.string.action_require_gps_info)
                .setPositiveButton(R.string.yes_info, (dialog, which) -> {
                    Intent enableGps = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    activity.startActivityForResult(enableGps, REQUEST_CODE_FINE_LOCATION_PERMISSION);
                })
                .setNeutralButton(R.string.no_info, (dialog, which) -> {
                    Toast.makeText(context, R.string.gps_disable_info, Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                })
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void localizationNotSetAlert(Context context) {
        Log.d(TAG, "localizationNotSetAlert: called");
        alertDialog = new AlertDialog.Builder(context)
                .setView(R.layout.alert_turn_on_gps_layout)
                .show();
    }
}
