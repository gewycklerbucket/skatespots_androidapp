package gewyckler.com.skatespots.viewModels;

import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gewyckler.com.skatespots.model.SkateSpot;
import gewyckler.com.skatespots.model.enums.SkateSpotType;

public class SkateSpotSettingsViewModel extends ViewModel {
    private static final String TAG = "SkateSpotSettingsViewMo";

    private MutableLiveData<List<String>> selectedObstacles;
    private MutableLiveData<List<String>> selectedImageUriList;

    private String name;
    private String skateSpotType;
    private double longitude;
    private double latitude;
    private String address;
    private String city;
    private String country;
    private boolean paid;
    private boolean lightning;
    private boolean indoor;

    public void initSelectedObstacles() {
        Log.d(TAG, "initSelectedObstacles: called");
        if (selectedObstacles == null) {
            selectedObstacles = new MutableLiveData<>();
            selectedObstacles.setValue(new ArrayList<>());
        }
    }

    public LiveData<List<String>> getSelectedObstacles() {
        return selectedObstacles;
    }

    public void setSelectedObstacles(List<String> obstaclesFromAdapter) {
        selectedObstacles.setValue(obstaclesFromAdapter);
    }

    public void initSelectedImagesUriList() {
        Log.d(TAG, "initSelectedImages: called");
        if (selectedImageUriList == null) {
            selectedImageUriList = new MutableLiveData<>();
            selectedImageUriList.setValue(new ArrayList<>());
        }
    }

    public LiveData<List<String>> getSelectedImageUriList() {
        return selectedImageUriList;
    }

    public void setSelectedImageUriList(List<String> selectedImageUriList) {
        this.selectedImageUriList.setValue(selectedImageUriList);
    }

//    public String getSelectedImageString() {
//        Log.d(TAG, "getSelectedImageString: called");
//        selectedImageString = Base64.getEncoder().encodeToString(selectedImageUriList.toString().getBytes());
//        return selectedImageString;
//    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSkateSpotType(String skateSpotType) {
        this.skateSpotType = skateSpotType;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isLightning() {
        return lightning;
    }

    public void setLightning(boolean lightning) {
        this.lightning = lightning;
    }

    public boolean isIndoor() {
        return indoor;
    }

    public void setIndoor(boolean indoor) {
        this.indoor = indoor;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public SkateSpot createSkateSpotWithGivenData() {
        Log.d(TAG, "createSkateSpotWithGivenData: called");
        SkateSpot newSkateSpot = new SkateSpot();
        newSkateSpot.setName(name);
        newSkateSpot.setCity(city);
        newSkateSpot.setAddress(address);
        newSkateSpot.setCountry(country);
        newSkateSpot.setPaid(paid);
        newSkateSpot.setLightning(lightning);
        newSkateSpot.setIndoor(indoor);
        newSkateSpot.setAccepted(false);

        newSkateSpot.setLatX(latitude);
        newSkateSpot.setLonY(longitude);

        newSkateSpot.setListOfObstacle(getSelectedObstacles().getValue());

        if (selectedImageUriList.getValue() != null && !selectedImageUriList.getValue().isEmpty()) {
//            newSkateSpot.setImageString(getSelectedImageString());
            newSkateSpot.setImage(getSelectedImageUriList().getValue().get(0));
        }

        newSkateSpot.setSkateSpotType(skateSpotType);
        return newSkateSpot;
    }

    /**
     * Convert enum values from SkateSpotType Enum to String and replacing "_" (underscore sign) by " " (space).
     */
    public List<String> convertSkateSpotTypesToStringAndRemoveUnderscore() {
        Log.d(TAG, "convertSkateSpotTypesToStringAndRemoveUnderscore: called");
        List<String> skateSpotTypes = new ArrayList<>();
        for (SkateSpotType type : SkateSpotType.values()) {
            String rewriteType = type.name().replace('_', ' ');
            skateSpotTypes.add(rewriteType);
        }
        return skateSpotTypes;
    }

    public void getLocationFromCoordinates(Geocoder geocoder) throws IOException {
        List<Address> fromLocation = geocoder.getFromLocation(latitude, longitude, 1);
        country = fromLocation.get(0).getCountryName();
        city = fromLocation.get(0).getLocality();
        address = fromLocation.get(0).getAddressLine(0).concat("");
    }
}
