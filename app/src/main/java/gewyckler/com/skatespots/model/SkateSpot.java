package gewyckler.com.skatespots.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.List;

/**
 * Main class SkateSpot.
 */
public class SkateSpot implements Serializable {
    private Long skateSpotId;

    private String name;
    private String skateSpotType;

    private boolean paid;
    private boolean lightning;
    private boolean indoor;
    private boolean accepted;

    private String city;
    private String address;
    private String country;

    private Double latX;
    private Double lonY;

    private List<String> listOfObstacle;

    private String image;

    public Long getSkateSpotId() {
        return skateSpotId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkateSpotType() {
        return skateSpotType;
    }

    public void setSkateSpotType(String skateSpotType) {
        this.skateSpotType = skateSpotType;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isLightning() {
        return lightning;
    }

    public void setLightning(boolean lightning) {
        this.lightning = lightning;
    }

    public boolean isIndoor() {
        return indoor;
    }

    public void setIndoor(boolean indoor) {
        this.indoor = indoor;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatX() {
        return latX;
    }

    public void setLatX(Double latX) {
        this.latX = latX;
    }

    public Double getLonY() {
        return lonY;
    }

    public void setLonY(Double lonY) {
        this.lonY = lonY;
    }

    public List<String> getListOfObstacle() {
        return listOfObstacle;
    }

    public void setListOfObstacle(List<String> listOfObstacle) {
        this.listOfObstacle = listOfObstacle;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @NonNull
    @Override
    public String toString() {
        return "id: " + skateSpotId
                + "\nname: " + name
                + "\ntype: " + skateSpotType
                + "\npaid: " + paid
                + "\nilluminate: " + lightning
                + "\nindoor: " + indoor
                + "\ncity: " + city
                + "\nadress: " + address
                + "\ncountry: " + country
                + "\nlatX: " + latX
                + "\nlonY: " + lonY
                + "\nobstacles: " + listOfObstacle.toString()
                + "\nimage: " + image;
    }
}
