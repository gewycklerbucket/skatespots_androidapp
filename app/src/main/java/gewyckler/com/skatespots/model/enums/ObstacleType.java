package gewyckler.com.skatespots.model.enums;

import gewyckler.com.skatespots.R;

public enum ObstacleType {
    //TODO: Dorobić reszte ikon przeszkód. ALe jako SVG, czyli wszystkie trzeba pozmieniac i dodac w więksezj rozdziedzlości
    BANK("Bank", R.drawable.ic_bank),
    BOX("Box", R.drawable.ic_box),
    SLOPE_BOX("Slope Box", R.drawable.ic_slope_box),
    //  PYRAMID("PYRAMID", R.drawable.ic_pyramid),
    RAIL("Rail", R.drawable.ic_rail),
    SLOPE_RAIL("Slope Rail", R.drawable.ic_rail_slope),
    STAIRS("Stairs", R.drawable.ic_stairs),
    QUARTER_PIPE("Quarter", R.drawable.ic_quarterpipe),
    MINIRAMP("Mini Ramp", R.drawable.ic_miniramp);
//  BOWL("BOWL", "R.drawable.ic_bowl"),

    private final String type;
    private final Integer iconId;

    public String getType() {
        return type;
    }

    public Integer getIconId() {
        return iconId;
    }

    ObstacleType(String type, Integer iconId) {
        this.type = type;
        this.iconId = iconId;
    }
}
