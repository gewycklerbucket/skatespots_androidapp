package gewyckler.com.skatespots.interfaces;

import android.location.Address;

import java.util.List;

public interface ILocalizationByMarkerListener {
    void sentAddressList(List<Address> addressList);
}
