package gewyckler.com.skatespots.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.model.enums.ObstacleType;

public class ObstacleInSkateSpotAdapter extends RecyclerView.Adapter<ObstacleInSkateSpotAdapter.ViewHolder> {
    private static final String TAG = "ObstacleInSkateSpotAd";
    private List<String> selectedObstacle;
    private Context context;

    public ObstacleInSkateSpotAdapter(Context context, List<String> selectedObstacle) {
        this.selectedObstacle = selectedObstacle;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: called");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_obstacle_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called");
        selectedObstacle.get(position);
        ObstacleType obstacleType = ObstacleType.valueOf(selectedObstacle.get(position));

        holder.name.setText(obstacleType.getType());

        holder.imageView.setImageResource(obstacleType.getIconId());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return selectedObstacle.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            name = itemView.findViewById(R.id.single_obstacle_name);
        }
    }
}
