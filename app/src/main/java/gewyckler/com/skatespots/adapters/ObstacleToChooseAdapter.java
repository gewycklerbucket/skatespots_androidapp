package gewyckler.com.skatespots.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.model.enums.ObstacleType;

public class ObstacleToChooseAdapter extends RecyclerView.Adapter<ObstacleToChooseAdapter.ViewHolder> {
    private static final String TAG = "ObstacleCustomAdapter";

    private final ObstacleType[] obType = ObstacleType.values();
    private final Context context;
    private List<String> selectedObstacle;

    private Animation scaleUp, scaleDown;

    public ObstacleToChooseAdapter(Context context) {
        this.context = context;
    }

    public List<String> getSelectedObstacle() {
        return selectedObstacle;
    }

    public ObstacleType getItem(int position) {
        return obType[position];
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: called");

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_obstacle_item, parent, false);

        scaleUp = AnimationUtils.loadAnimation(view.getContext(), R.anim.scale_up);
        scaleDown = AnimationUtils.loadAnimation(view.getContext(), R.anim.scale_down);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called");

        holder.name.setText(obType[position].getType());
        holder.imageView.setImageDrawable(AppCompatResources.getDrawable(context, obType[position].getIconId()));

        if (selectedObstacle.contains(getItem(position).name())) {
            //Button pressed
            holder.checkBox.setChecked(true);
            holder.checkBox.toggle();
            holder.itemView.setElevation(0f);
            holder.itemView.startAnimation(scaleUp);

        } else {
            holder.checkBox.setChecked(false);
        }

        int postitionInternal = position;
        holder.itemView.setOnClickListener(buttonView -> {
            if (selectedObstacle.contains(ObstacleToChooseAdapter.this.getItem(postitionInternal).name())) {
                selectedObstacle.remove(ObstacleToChooseAdapter.this.getItem(postitionInternal).name());
                holder.checkBox.toggle();
                buttonView.setElevation(4f);
                buttonView.startAnimation(scaleDown);

                Toast.makeText(context, selectedObstacle.toString(), Toast.LENGTH_SHORT).show();
                return;
            }
            if (!selectedObstacle.contains(ObstacleToChooseAdapter.this.getItem(postitionInternal).name())) {
                selectedObstacle.add(ObstacleToChooseAdapter.this.getItem(postitionInternal).name());
                holder.checkBox.toggle();
                buttonView.setElevation(0f);
                buttonView.startAnimation(scaleUp);

                Toast.makeText(context, selectedObstacle.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return obType.length;
    }

    public void setObstacles(List<String> selectedObstacle) {
        this.selectedObstacle = selectedObstacle;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imageView;
        private final TextView name;
        private final CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            name = itemView.findViewById(R.id.single_obstacle_name);
            checkBox = itemView.findViewById(R.id.checkbox);
        }
    }
}
