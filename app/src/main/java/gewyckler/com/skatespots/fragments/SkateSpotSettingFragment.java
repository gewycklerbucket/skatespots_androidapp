package gewyckler.com.skatespots.fragments;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;
import static gewyckler.com.skatespots.utilities.Alerts.gpsIsNotEnableAlert;
import static gewyckler.com.skatespots.utilities.Alerts.localizationNotSetAlert;
import static gewyckler.com.skatespots.utilities.Constants.SKATESPOT_DEFAULT_NAME;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.adapters.ObstacleToChooseAdapter;
import gewyckler.com.skatespots.adapters.PhotoRecyclerViewAdapter;
import gewyckler.com.skatespots.model.SkateSpot;
import gewyckler.com.skatespots.service.SkatePointApiService;
import gewyckler.com.skatespots.utilities.Alerts;
import gewyckler.com.skatespots.viewModels.SkateSpotSettingsViewModel;

public class SkateSpotSettingFragment extends Fragment {
    private static final String TAG = "SkateSpotSettingsFrag";
    private static final String OBSTACLES_CODE = "OBSTACLES_CODE";
    private static final String IMAGES_CODE = "IMAGES_CODE";

    private SkateSpotSettingsViewModel skateSpotSettingsViewModel;

    private View view;
    private Context context;
    private Activity activity;

    private CheckBox paidCheckBox;
    private CheckBox lightningCheckBox;
    private CheckBox indoorCheckBox;
    private TextView localizationInfoLabel;
    private TextView localizationInfo;
    private EditText nameEditText;
    private Spinner skateSpotTypeSpinner;

    private List<String> selectedObstacles;
    private List<String> selectedImagesUri;
    private RecyclerView obstacleRecyclerView;
    private RecyclerView photoRecyclerView;


    private ObstacleToChooseAdapter obstacleCustomAdapter;
    private PhotoRecyclerViewAdapter photoRecyclerViewAdapter;

    public ActivityResultLauncher<String> permissionLauncher;
    private ActivityResultLauncher<Intent> activityLauncher;

    private Animation scaleUp, scaleDown;

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(TAG, "onAttach: called");
        super.onAttach(context);

        this.context = context;
        this.activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: called");

        skateSpotSettingsViewModel = new ViewModelProvider(this).get(SkateSpotSettingsViewModel.class);
        skateSpotSettingsViewModel.initSelectedObstacles();
        skateSpotSettingsViewModel.initSelectedImagesUriList();

        view = inflater.inflate(R.layout.skatespot_settings_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: called");

        if (savedInstanceState != null) {
            if (savedInstanceState.getSerializable(OBSTACLES_CODE) != null) {
                selectedObstacles = (List<String>) savedInstanceState.getSerializable(OBSTACLES_CODE);
                skateSpotSettingsViewModel.setSelectedObstacles(selectedObstacles);
            }
            if (savedInstanceState.getSerializable(IMAGES_CODE) != null) {
                selectedImagesUri = (List<String>) savedInstanceState.getSerializable(IMAGES_CODE);
                skateSpotSettingsViewModel.setSelectedImageUriList(selectedImagesUri);
            }
        }

        viewInit();
        initLauncher();
        fillLocalizationInfoFields();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        selectedObstacles = obstacleCustomAdapter.getSelectedObstacle();
        outState.putSerializable(OBSTACLES_CODE, (Serializable) selectedObstacles);

        selectedImagesUri = photoRecyclerViewAdapter.getSelectedImagesUri();
        outState.putSerializable(IMAGES_CODE, (Serializable) selectedImagesUri);

        super.onSaveInstanceState(outState);
    }

    @SuppressLint("NewApi")
    private void viewInit() {
        paidCheckBox = view.findViewById(R.id.paidCheckBox);
        lightningCheckBox = view.findViewById(R.id.illuminatedCheckBox);
        indoorCheckBox = view.findViewById(R.id.indoorCheckBox);

        ImageButton localizationButton = view.findViewById(R.id.get_localization_from_gps);
        localizationButton.setOnClickListener(v -> getCurrentLocation());

        localizationInfoLabel = view.findViewById(R.id.localization_info_label);
        localizationInfo = view.findViewById(R.id.localization_info);
        localizationInfo.setText(skateSpotSettingsViewModel.getAddress());

        nameEditText = view.findViewById(R.id.name_edittext);

        skateSpotTypeSpinner = view.findViewById(R.id.skateSpot_type_spinner);
        initSkateSpotTypeSpinnerAdapter();

        Button createSkateSpotButton = view.findViewById(R.id.create_skatespot_button);

        obstacleRecyclerView = view.findViewById(R.id.obstacle_recycler_view);
        initObstacleRecycleView();
        skateSpotSettingsViewModel.getSelectedObstacles().observe(getActivity(), obstacleList -> {
            Log.d(TAG, "onChanged: Obstacle Custom Adapter is observed");
            obstacleCustomAdapter.setObstacles(obstacleList);
        });

        photoRecyclerView = view.findViewById(R.id.photos_recycleview);
        initPhotoRecycleView();
        skateSpotSettingsViewModel.getSelectedImageUriList().observe(getActivity(), uriList -> {
            Log.d(TAG, "onChanged: Photo in View Model is observed");
            photoRecyclerViewAdapter.setImageStringList(uriList);
        });

        createSkateSpotButton.setOnClickListener(v -> {
            skateSpotSettingsViewModel.setPaid(paidCheckBox.isChecked());
            skateSpotSettingsViewModel.setLightning(lightningCheckBox.isChecked());
            skateSpotSettingsViewModel.setIndoor(indoorCheckBox.isChecked());
            skateSpotSettingsViewModel.setName(nameEditText.getText().toString().isEmpty() ? SKATESPOT_DEFAULT_NAME : nameEditText.getText().toString());
            skateSpotSettingsViewModel.setSkateSpotType(skateSpotTypeSpinner.getSelectedItem().toString());

            SkateSpot newSkateSpot = skateSpotSettingsViewModel.createSkateSpotWithGivenData();
            Log.i(TAG, "Thread: " + Thread.currentThread().getName() + " - Creating SkateSpot");
            if (validateLocalizationData()) {
                if (validateObstacleList(newSkateSpot)) {
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            synchronized (this) {
                                SkatePointApiService.getInstance().createSkateSpotRequest(newSkateSpot);
                            }
                            Log.i(TAG, " Thread: " + Thread.currentThread().getName() + " - SkateSpot Created");
                        }
                    };
                    Thread thread = new Thread(runnable);
                    thread.start();
                    Toast.makeText(context, R.string.skateSpot_created_info, Toast.LENGTH_SHORT).show();
                    activity.finish();
                }
            }
        });
    }

    private void initPhotoRecycleView() {
        Log.d(TAG, "initRecycleView: called: init photo recyclerview");
        photoRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
        photoRecyclerView.setLayoutManager(layoutManager);

        photoRecyclerViewAdapter = new PhotoRecyclerViewAdapter();
//        photoRecyclerViewAdapter.initData();

        photoRecyclerView.setAdapter(photoRecyclerViewAdapter);
    }

    public void setImagesList(List<String> imageUriList) {
        skateSpotSettingsViewModel.setSelectedImageUriList(imageUriList);
    }

    private void initObstacleRecycleView() {
        Log.d(TAG, "initObstacleRecycleView: called: init obstacle recyclerview");
        obstacleRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 4, RecyclerView.VERTICAL, false);

        obstacleRecyclerView.setLayoutManager(layoutManager);
        obstacleCustomAdapter = new ObstacleToChooseAdapter(context);

        obstacleRecyclerView.setAdapter(obstacleCustomAdapter);
    }

    public void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        activityLauncher.launch(Intent.createChooser(intent, "Select Image"));
    }

    /**
     * Prevent against adding skatespot without any obstacle
     */
    private boolean validateObstacleList(SkateSpot newSkateSpot) {
        if (newSkateSpot.getListOfObstacle().isEmpty()) {
            Toast.makeText(context, R.string.choose_obstacles, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Method initialize SpinnerAdapter, add values from SkateSpotType Enum (converted to String).
     * Next it is setting created SpinnerAdapter name "skateSpotTypeSpinnerAdapter" as a skateSpotTypeSpinner's adapter.
     */
    private void initSkateSpotTypeSpinnerAdapter() {
        List<String> skateSpotTypes = skateSpotSettingsViewModel.convertSkateSpotTypesToStringAndRemoveUnderscore();
        ArrayAdapter<String> skateSpotTypeSpinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, skateSpotTypes);

        skateSpotTypeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        skateSpotTypeSpinner.setAdapter(skateSpotTypeSpinnerAdapter);
    }

    private void initLauncher() {
        permissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(),
                permission -> {
                    if (permission) {
                        Toast.makeText(context, R.string.permission_granted_info, Toast.LENGTH_SHORT).show();
                    } else {
                        Alerts.permissionNotGrantedAlert(context);
                    }
                });

        activityLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                Uri uri = result.getData().getData();

                List<String> lS = new ArrayList<>();
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "run: compressing file. Thread Name1: " + Thread.currentThread().getName());
                        synchronized (this) {
                            try {

                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);

                                Matrix matrix = new Matrix();
                                float scaleWidth = ((float) 200) / bitmap.getWidth();
                                float scaleHeight = ((float) 200) / bitmap.getHeight();
                                matrix.postScale(scaleWidth, scaleHeight);
                                matrix.postRotate(45);

                                Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight());
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);

                                byte[] bytes = stream.toByteArray();

                                String s = Base64.encodeToString(bytes, Base64.DEFAULT);
                                s = s.replace("\n", "");

                                lS.add(s);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        handler.post(() -> {
                            Toast.makeText(getContext(), getString(R.string.photo_loaded) + lS.size(), Toast.LENGTH_SHORT).show();
                            setImagesList(lS);
                        });
                        Log.i(TAG, "run: Compressing ended");
                    }

                };
                Log.i(TAG, "Thread Name1: " + Thread.currentThread().getName());
                Thread thread = new Thread(runnable);
                thread.start();
            }
        });
    }

    /**
     * Method is checking that input given to field "EditText city;" and return true if meets the conditions.
     */
    @RequiresApi(api = Build.VERSION_CODES.P)
    private boolean validateLocalizationData() {
        if (localizationInfo.getText().length() <= 0) {
            localizationInfoLabel.setTextColor(getResources().getColor(R.color.red));
            localizationNotSetAlert(activity);
            return false;
        }
        return true;
    }

    /**
     * Convert enum values from SkateSpotType Enum to String and replacing "_" (underscore sign) by " " (space).
     */

    @RequiresApi(api = Build.VERSION_CODES.P)
    private boolean checkIfGpsIsEnable() {
        LocationManager locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            gpsIsNotEnableAlert(context, activity);
            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void getCurrentLocation() {
        if (ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (checkIfGpsIsEnable()) {
                LocationRequest locationRequest = new LocationRequest()
                        .setInterval(100)
                        .setFastestInterval(100)
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                LocationServices.getFusedLocationProviderClient(activity)
                        .requestLocationUpdates(locationRequest, new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                super.onLocationResult(locationResult);
                                if (locationResult != null && locationResult.getLocations().size() > 0) {
                                    int latestLocationIndex = locationResult.getLocations().size() - 1;

                                    skateSpotSettingsViewModel.setLatitude(locationResult.getLocations().get(latestLocationIndex).getLatitude());
                                    skateSpotSettingsViewModel.setLongitude(locationResult.getLocations().get(latestLocationIndex).getLongitude());

                                    getAddress();
                                    fillLocalizationInfoFields();

                                    LocationServices.getFusedLocationProviderClient(activity)
                                            .removeLocationUpdates(this);
                                }
                            }
                        }, Looper.getMainLooper());
                Toast.makeText(activity, R.string.location_is_set, Toast.LENGTH_SHORT).show();
            }

        } else {
            permissionLauncher.launch(ACCESS_FINE_LOCATION);
        }

    }

    private void getAddress() {
        Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
        try {
            skateSpotSettingsViewModel.getLocationFromCoordinates(geocoder);

        } catch (IOException e) {
            Log.e(TAG, "getAddress: No address found", new IOException());
            Toast.makeText(activity, R.string.no_addres_found, Toast.LENGTH_SHORT).show();
        }
    }

    public void setLocalizationInViewModel(List<Address> addressList) {
        skateSpotSettingsViewModel.setLatitude(addressList.get(0).getLatitude());
        skateSpotSettingsViewModel.setLongitude(addressList.get(0).getLongitude());
        skateSpotSettingsViewModel.setAddress(addressList.get(0).getAddressLine(0));
        skateSpotSettingsViewModel.setCountry(addressList.get(0).getCountryName());
        skateSpotSettingsViewModel.setCity(addressList.get(0).getLocality());
    }

    public void fillLocalizationInfoFields() {
        localizationInfoLabel.setTextColor(getResources().getColor(R.color.dark_grey));
        localizationInfo.setText(skateSpotSettingsViewModel.getAddress());
        localizationInfo.setTextColor(getResources().getColor(R.color.green));
    }
}

