package gewyckler.com.skatespots.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import gewyckler.com.skatespots.model.SkateSpot;
import gewyckler.com.skatespots.utilities.ClusterMarker;

public class MapViewModel extends ViewModel {

    private MutableLiveData<List<SkateSpot>> skateSpotList;
    private MutableLiveData<ClusterMarker> selectedMarker;

    public LiveData<List<SkateSpot>> getSkateSpotList() {
        return skateSpotList;
    }

    public void setSkateSpotList(List<SkateSpot> skateSpots) {
        skateSpotList.setValue(skateSpots);
    }

    public LiveData<ClusterMarker> getSelectedClusterMarker() {
        return selectedMarker;
    }

    public void setSelectedClusterMarker(ClusterMarker marker) {
        selectedMarker.setValue(marker);
    }

    public void initMapViewModelData() {
        initSkateSpotList();
        initSelectedMarker();
    }

    private void initSkateSpotList() {
        if (skateSpotList == null) {
            skateSpotList = new MutableLiveData<>();
            skateSpotList.setValue(new ArrayList<>());
        }
    }

    private void initSelectedMarker() {
        if (selectedMarker == null) {
            selectedMarker = new MutableLiveData<>();
        }
    }
}
