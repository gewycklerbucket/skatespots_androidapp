package gewyckler.com.skatespots.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

public class Permissions {

    private static final String TAG = "Permissions";

    public static void getInternetPermission(Activity activity, Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "getInternetPermission: Permission granted");
            Constants.INTERNET_PERMISSION_GRANTED = true;
        } else {
            Log.d(TAG, "getInternetPermission: Permission not granted");
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.INTERNET}, Constants.REQUEST_CODE_INTERNET_PERMISSION);
        }
    }
}
