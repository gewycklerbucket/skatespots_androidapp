package gewyckler.com.skatespots.fragments;

import static gewyckler.com.skatespots.utilities.Constants.DELETED_SKATESPOT_ID;
import static gewyckler.com.skatespots.utilities.Constants.REQUEST_CODE_DELETE_SKATESPOT;
import static gewyckler.com.skatespots.utilities.Constants.SKATESPOT_DEFAULT_NAME;
import static gewyckler.com.skatespots.utilities.Constants.SKATESPOT_DETAILS;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.OutputStream;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.activities.SkateSpotDetailsActivity;
import gewyckler.com.skatespots.interfaces.IMainMenuMapListener;
import gewyckler.com.skatespots.model.SkateSpot;
import gewyckler.com.skatespots.service.SkatePointApiService;

public class SkateSpotDetailsWindowFragment extends Fragment {

    private final static String TAG = "SkateSpotDetWindowFRG";
    private final static String SKATESPOT_KEY = "SKATESPOT";

    private View view;
    private IMainMenuMapListener iMainMenuMapListener;

    private SkateSpot skateSpot;

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(TAG, "onAttach: called");
        super.onAttach(context);
        if (context instanceof IMainMenuMapListener) {
            iMainMenuMapListener = (IMainMenuMapListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: called");
        if (savedInstanceState != null) {
            skateSpot = (SkateSpot) savedInstanceState.getSerializable(SKATESPOT_KEY);
        }
        view = inflater.inflate(R.layout.skatespot_details_window, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: called");
        viewInit();
    }

    public void setSkateSpot(SkateSpot skateSpot) {
        this.skateSpot = skateSpot;
    }

    private void viewInit() {
        Log.d(TAG, "viewInit: called");

        ImageView tvImage = view.findViewById(R.id.detail_skateSpotImage);
        String image = skateSpot.getImage();
        if (image != null && !image.isEmpty()) {
            byte[] bytes = Base64.decode(image, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length - 1);
            if (bitmap != null) {
                loadBitmapByPicasso(tvImage.getContext(), bitmap, tvImage);
//                tvImage.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
                Toast.makeText(getContext(), "Bitmap found", Toast.LENGTH_LONG).show();
            } else {

                Toast.makeText(getContext(), "Bitmap NOT found", Toast.LENGTH_LONG).show();
            }
        }

        TextView tvName = view.findViewById(R.id.detail_skateSpotName);
        String name = skateSpot.getName();
        tvName.setText(name != null && name.isEmpty() ? SKATESPOT_DEFAULT_NAME : name);

        TextView tvAddress = view.findViewById(R.id.detail_address);
        tvAddress.setText(skateSpot.getAddress());

        ActivityResultLauncher<Intent> activityLauncher = getActivityLauncher();
        view.setOnClickListener(v -> {
            Intent skateSpotDetailsIntent = new Intent(view.getContext(), SkateSpotDetailsActivity.class);
            skateSpotDetailsIntent.putExtra(SKATESPOT_DETAILS, skateSpot);
            activityLauncher.launch(skateSpotDetailsIntent);
        });
    }

    private void loadBitmapByPicasso(Context pContext, Bitmap pBitmap, ImageView pImageView) {
        try {
            Uri uri = Uri.fromFile(File.createTempFile("temp_file_name", ".jpg", pContext.getCacheDir()));
            OutputStream outputStream = pContext.getContentResolver().openOutputStream(uri);
            pBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();
            Picasso.get().load(uri).into(pImageView);
        } catch (Exception e) {
            Log.e("LoadBitmapByPicasso", e.getMessage());
        }
    }

    private ActivityResultLauncher<Intent> getActivityLauncher() {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == REQUEST_CODE_DELETE_SKATESPOT) {
                        Log.d(TAG, "onActivityResult: Add Skatespot Request called");
                        Long skateSpotId = (Long) result.getData().getExtras().get(DELETED_SKATESPOT_ID);

                        if (skateSpotId != null) {
                            SkatePointApiService.getInstance().deleteSkateSpot(skateSpotId);
                            Toast.makeText(getActivity(), R.string.skateSpot_deleted_info, Toast.LENGTH_SHORT).show();
                            iMainMenuMapListener.removeCurrentSkateSpotDetailsWindow();
                            iMainMenuMapListener.removeSkateSpotFromList(skateSpotId);
                        }
                    }
                });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SKATESPOT_KEY, skateSpot);
    }
}
