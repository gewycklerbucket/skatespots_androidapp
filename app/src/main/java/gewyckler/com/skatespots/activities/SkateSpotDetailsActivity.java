package gewyckler.com.skatespots.activities;

import static gewyckler.com.skatespots.utilities.Constants.DELETED_SKATESPOT_ID;
import static gewyckler.com.skatespots.utilities.Constants.REQUEST_CODE_DELETE_SKATESPOT;
import static gewyckler.com.skatespots.utilities.Constants.SKATESPOT_DETAILS;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.OutputStream;
import java.util.List;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.adapters.ObstacleInSkateSpotAdapter;
import gewyckler.com.skatespots.model.SkateSpot;
import gewyckler.com.skatespots.service.SkatePointApiService;

public class SkateSpotDetailsActivity extends AppCompatActivity {
    private static final String TAG = "SkateSpotDetailsAct";

    private Toolbar toolbar;

    private TextView name;
    private ImageView imageView;
    private TextView country;
    private TextView city;
    private TextView address;
    private SkateSpot givenSkateSpot;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ObstacleInSkateSpotAdapter obstacleInSkateSpotAdapter;

    private SkatePointApiService skatePointApiService;

    private TextView tempText;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called");
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.skatespot_details_layout);
        this.setTitle(getResources().getString(R.string.details));
        obtainExtras();
        viewInit();

    }

    private void obtainExtras() {
        Log.d(TAG, "obtainExtras: called");
        givenSkateSpot = (SkateSpot) getIntent().getExtras().get(SKATESPOT_DETAILS);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.skatespot_detail_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_skateSpot:
                Log.d(TAG, "onOptionsItemSelected: delete called");
                Intent intent = new Intent();
                if (givenSkateSpot.getSkateSpotId() != null) {
                    intent.putExtra(DELETED_SKATESPOT_ID, givenSkateSpot.getSkateSpotId());
                    setResult(REQUEST_CODE_DELETE_SKATESPOT, intent);
                }
                finish();
                return false;

            default:
                Log.d(TAG, "onOptionsItemSelected: default called");
                return super.onOptionsItemSelected(item);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void viewInit() {
        Log.d(TAG, "viewInit: called");

        toolbar = findViewById(R.id.skatespot_detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        imageView = findViewById(R.id.detail_photo);
        String imageString = givenSkateSpot.getImage();
        if (imageString != null && !imageString.isEmpty()) {
            byte[] bytes = Base64.decode(imageString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            if (bitmap != null) {
                loadBitmapByPicasso(imageView.getContext(), bitmap, imageView);
            }
        }

        name = findViewById(R.id.detail_skateSpotName);
        name.setText(givenSkateSpot.getName());

        address = findViewById(R.id.detail_address);
        address.setText(givenSkateSpot.getAddress());

        recyclerView = findViewById(R.id.obstacle_recycler_view);
        initObstacleRecycleView(givenSkateSpot.getListOfObstacle());

    }

    private void initObstacleRecycleView(List<String> obstacleList) {
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this, 4);

        recyclerView.setLayoutManager(layoutManager);
        obstacleInSkateSpotAdapter = new ObstacleInSkateSpotAdapter(this, obstacleList);

        recyclerView.setAdapter(obstacleInSkateSpotAdapter);
    }

    private void loadBitmapByPicasso(Context pContext, Bitmap pBitmap, ImageView pImageView) {
        try {
            Uri uri = Uri.fromFile(File.createTempFile("temp_file_name", ".jpg", pContext.getCacheDir()));
            OutputStream outputStream = pContext.getContentResolver().openOutputStream(uri);
            pBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();
            Picasso.get().load(uri).into(pImageView);
        } catch (Exception e) {
            Log.e("LoadBitmapByPicasso", e.getMessage());
        }
    }
}
