package gewyckler.com.skatespots.activities;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static gewyckler.com.skatespots.utilities.Constants.ERROR_DIALOG_REQUEST;
import static gewyckler.com.skatespots.utilities.Constants.MAP_FRAGMENT_TAG;
import static gewyckler.com.skatespots.utilities.Constants.SKATESPOT_DETAILS_WINDOW_FRAGMENT_TAG;
import static gewyckler.com.skatespots.utilities.Constants.USER_POSITION_ZOOM;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.List;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.fragments.MapFragment;
import gewyckler.com.skatespots.fragments.SkateSpotDetailsWindowFragment;
import gewyckler.com.skatespots.interfaces.IMainMenuMapListener;
import gewyckler.com.skatespots.model.SkateSpot;
import gewyckler.com.skatespots.service.SkatePointApiService;
import gewyckler.com.skatespots.utilities.Alerts;
import gewyckler.com.skatespots.utilities.Permissions;
import gewyckler.com.skatespots.viewModels.MainMenuViewModel;

/**
 * Main menu activity.
 * First what user see after starting the app.
 */
public class MainMenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        IMainMenuMapListener {

    private static final String TAG = "MainMenuActivity";

    private MainMenuViewModel mainMenuViewModel;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;

    private MapFragment mapFragment;
    private SkateSpotDetailsWindowFragment skateSpotDetailsWindowFragment;

    private FloatingActionButton getUserLocalization;

    private ActivityResultLauncher<String> permissionLauncher;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        setTitle(getResources().getString(R.string.app_name));
        mainMenuViewModel = new ViewModelProvider(this).get(MainMenuViewModel.class);

        if (savedInstanceState != null) {
            mapFragment = (MapFragment) getSupportFragmentManager().findFragmentByTag(MAP_FRAGMENT_TAG);
            skateSpotDetailsWindowFragment = (SkateSpotDetailsWindowFragment) getSupportFragmentManager().findFragmentByTag(SKATESPOT_DETAILS_WINDOW_FRAGMENT_TAG);

        } else if (mapFragment == null) {
            mapFragment = new MapFragment();
        }

        initLauncher();
        viewInit();
        askForNecessaryPermissions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu: called");
        getMenuInflater().inflate(R.menu.main_top_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: called");
        switch (item.getItemId()) {
            case R.id.add_skatespot:
                Intent addSkateSpotIntent = new Intent(this, AddSkateSpotActivity.class);
                startActivity(addSkateSpotIntent);
                return false;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.d(TAG, "onNavigationItemSelected: called");
        int id = item.getItemId();
        if (id == R.id.get_all_button) {
            Permissions.getInternetPermission(this, MainMenuActivity.this);
            setSkateSpotList(SkatePointApiService.getInstance().getAllSkateSpotRequest());
        }
        if (id == R.id.get_skateparks_button) {
            Permissions.getInternetPermission(this, MainMenuActivity.this);
            setSkateSpotList(SkatePointApiService.getInstance().getParkSpotsRequest());
        }
        if (id == R.id.get_streetspots_button) {
            Permissions.getInternetPermission(this, MainMenuActivity.this);
            setSkateSpotList(SkatePointApiService.getInstance().getStreetSpotsRequest());
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setSkateSpotList(List<SkateSpot> skateSpotList) {
        mapFragment.setSkateSpotList(skateSpotList);
    }

    private void viewInit() {
        Log.d(TAG, "viewInit: called");
        setUpNavigationBar();

        initMapFragment();

        getUserLocalization = findViewById(R.id.zoom_to_localization);
        getUserLocalization.setOnClickListener(v -> getDeviceLocation());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initLauncher() {
        permissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(),
                permission -> {
                    if (!permission) {
                        Alerts.permissionNotGrantedAlert(this);
                    }
                });
    }

    private void setUpNavigationBar() {
        Log.d(TAG, "setUpNavigationBar: called");
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void initMapFragment() {
        Log.d(TAG, "initMapFragment: called");
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.localization_by_marker_frame_layout, mapFragment, MAP_FRAGMENT_TAG)
                .commit();
    }

    private boolean isServiceOk() {
        Log.d(TAG, "isServiceOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MainMenuActivity.this);
        if (available == ConnectionResult.SUCCESS) {
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            Log.d(TAG, "isServicesOK: an error occurred but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(MainMenuActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void askForNecessaryPermissions() {
        Log.d(TAG, "askForNecessaryPermissions: called");
        if (isServiceOk()) {
            Log.d(TAG, "askForNecessaryPermissions: service is OK");
            permissionLauncher.launch(Manifest.permission.INTERNET);
            permissionLauncher.launch(ACCESS_FINE_LOCATION);
        }
    }

    public boolean checkIfGpsIsEnable() {
        Log.d(TAG, "checkIfGpsIsEnable: called");
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.d(TAG, "checkIfGpsIsEnable: GPS is disable");
            Alerts.gpsIsNotEnableAlert(MainMenuActivity.this, MainMenuActivity.this);
            return false;
        }
        Log.d(TAG, "checkIfGpsIsEnable: GPS is enable");
        return true;
    }

    public void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation: called");
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "getDeviceLocation: Access Fine Location Permission Granted");
            if (checkIfGpsIsEnable()) {
                LocationRequest locationRequest = new LocationRequest()
                        .setInterval(500)
                        .setFastestInterval(100)
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                LocationServices.getFusedLocationProviderClient(this)
                        .requestLocationUpdates(locationRequest, new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                super.onLocationResult(locationResult);
                                if (locationResult != null && locationResult.getLocations().size() > 0) {
                                    Log.d(TAG, "onLocationResult: location result not null && not empty");
                                    int latestLocationIndex = locationResult.getLocations().size() - 1;

                                    mainMenuViewModel.setLatitude(locationResult.getLocations().get(latestLocationIndex).getLatitude());
                                    mainMenuViewModel.setLongitude(locationResult.getLocations().get(latestLocationIndex).getLongitude());

                                    zoomToDeviceLocation();

                                    LocationServices.getFusedLocationProviderClient(MainMenuActivity.this)
                                            .removeLocationUpdates(this);
                                }
                            }
                        }, Looper.getMainLooper());
            }

        } else {
            Log.d(TAG, "getDeviceLocation: Access Fine Location Permission Not Granted");
            permissionLauncher.launch(ACCESS_FINE_LOCATION);
        }
    }

    public void zoomToDeviceLocation() {
        Log.d(TAG, "zoomToDeviceLocation: called");
        LatLng latLng = new LatLng(mainMenuViewModel.getLatitude(), mainMenuViewModel.getLongitude());
        mapFragment.moveCamera(latLng, USER_POSITION_ZOOM);
    }

    @Override
    public void initSkateSpotDetailsWindow(SkateSpot skateSpot) {
        Log.d(TAG, "initSkateSpotDetailWindowFragment: called");
        skateSpotDetailsWindowFragment = new SkateSpotDetailsWindowFragment();
        skateSpotDetailsWindowFragment.setSkateSpot(skateSpot);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.skatespot_details_window_frame_layout, skateSpotDetailsWindowFragment, SKATESPOT_DETAILS_WINDOW_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void removeCurrentSkateSpotDetailsWindow() {
        if (skateSpotDetailsWindowFragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(skateSpotDetailsWindowFragment)
                    .commit();
        }
    }

    @Override
    public void removeSkateSpotFromList(Long id) {
        mapFragment.removeSkateSpotFromList(id);
    }
}



