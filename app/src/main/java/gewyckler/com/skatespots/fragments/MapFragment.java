package gewyckler.com.skatespots.fragments;

import static gewyckler.com.skatespots.utilities.Constants.CENTER_OF_POLAND;
import static gewyckler.com.skatespots.utilities.Constants.CLUSTER_MARGIN;
import static gewyckler.com.skatespots.utilities.Constants.DEFAULT_ZOOM;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.interfaces.ILocalizationByMarkerListener;
import gewyckler.com.skatespots.interfaces.IMainMenuMapListener;
import gewyckler.com.skatespots.model.SkateSpot;
import gewyckler.com.skatespots.utilities.ClusterMarker;
import gewyckler.com.skatespots.viewModels.MapViewModel;

/**
 * ###LOADED BY MainMenuActivity###
 * If Class is loaded by MainMenuActivity the methods from IMainMenuMapListener are implemented.
 * - void removeCurrentSkateSpotDetailsWindow();
 * - void initSkateSpotDetailsWindow(SkateSpot skateSpot);
 * <p>
 * Class is responsible for displaying map fragment (using GoogleMap SDK) in MainMenuActivity,
 * and for displaying all markers and handling all listeners connected with them.
 * <p>
 * From here user can:
 * - click on marker to display SkateSpotDetailsWindowFragment(InfoWindow) (basic information in upper part of the screen),
 * - click on InfoWindow to start SkateSpotDetailActivity (to see created SkateSpot details),
 * <p>
 * <p>
 * ###LOADED BY AddSkateSpotActivity###
 * If MapFragment is entered from AddSkateSpotActivity the methods from ILocalizationByMarkerListener are implemented.
 * - void sentAddressList(List<Address> addressList);
 * <p>
 * Than Class enable user to choose location of SkateSpot by putting marker on map. In this case implemented method GoogleMap.OnMapLongClickListener
 * and GoogleMap.OnMarkerDragListener are used.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerDragListener, ClusterManager.OnClusterClickListener<ClusterMarker>,
        ClusterManager.OnClusterItemClickListener<ClusterMarker> {

    private final static String TAG = "MapFragment";
    private MapViewModel mapViewModel;
    private GoogleMap mMap;
    private CameraPosition cameraPosition;

    private Geocoder geocoder;
    private List<Address> fromLocation;

    private IMainMenuMapListener iMainMenuMapListener;
    private ILocalizationByMarkerListener iLocalizationByMarkerListener;
    private Context context;

    private ClusterManager<ClusterMarker> clusterManager;

    private final String CAMERA_POSITION = "CAMERA_POSITION";

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IMainMenuMapListener) {
            iMainMenuMapListener = (IMainMenuMapListener) context;

        } else if (context instanceof ILocalizationByMarkerListener) {
            iLocalizationByMarkerListener = (ILocalizationByMarkerListener) context;
            geocoder = new Geocoder(context, Locale.getDefault());
        } else {
            Log.e(TAG, "onAttach: " + context.toString() + "MapFragment must implement IMainMenuMapListener or ILocalizationByMarkerListener", new RuntimeException());
        }
        this.context = context;
    }

    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: called");
        if (savedInstanceState != null) {
            cameraPosition = (CameraPosition) savedInstanceState.get(CAMERA_POSITION);
        } else {
            cameraPosition = new CameraPosition(CENTER_OF_POLAND, DEFAULT_ZOOM, DEFAULT_ZOOM, DEFAULT_ZOOM);
        }
//        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);

        View view = null;
        if (inflater != null) {
            view = inflater.inflate(R.layout.fragment_map, container, false);
        }
        SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.google_map);

        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(this);
        }

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        iMainMenuMapListener = null;
        iLocalizationByMarkerListener = null;
    }

    //GOOGLE MAP
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: called");
        this.mMap = googleMap;
        if (context instanceof IMainMenuMapListener) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onMapReady: Permission Granted");
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);

            }
            mapViewModel.initMapViewModelData();

            mapViewModel.getSkateSpotList().observe(this, skateSpots -> {
                Log.d(TAG, "onChanged: SkateSpot list is observed");
                setUpClustersSettings();
                putMarksOnMap();
            });

            mapViewModel.getSelectedClusterMarker().observe(this, clusterMarker -> {
                Log.d(TAG, "onMapReady: Cluster Marker is observed");
                try {
                    iMainMenuMapListener.initSkateSpotDetailsWindow(clusterMarker.getSkateSpot());
                } catch (NullPointerException npe) {
                    iMainMenuMapListener.removeCurrentSkateSpotDetailsWindow();
                }
            });
        }

        if (context instanceof ILocalizationByMarkerListener) {
            mMap.setOnMapLongClickListener(this);
            mMap.setOnMarkerDragListener(this);
            mMap.getUiSettings().setCompassEnabled(true);
        }

        moveCamera(cameraPosition.target, cameraPosition.zoom);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        try {
            fromLocation = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            Address address = fromLocation.get(0);
            String streetAddress = address.getAddressLine(0);
            mMap.clear();
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(streetAddress)
                    .draggable(true));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: called");
        super.onPause();
    }

    public void putMarksOnMap() {
        Log.d(TAG, "putMarksOnMap: called");
        mMap.clear();
        List<SkateSpot> skateSpotList = mapViewModel.getSkateSpotList().getValue();
        if (skateSpotList != null && !skateSpotList.isEmpty()) {
            for (SkateSpot skateSpot : skateSpotList) {

                ClusterMarker offsetItem = new ClusterMarker(
                        new LatLng(skateSpot.getLatX(),
                                skateSpot.getLonY()),
                        "",
                        "",
                        skateSpot);

                clusterManager.addItem(offsetItem);
            }
            clusterManager.cluster();
            Toast.makeText(context, getString(R.string.found_info) + skateSpotList.size()
                    + getString(R.string.objects_info), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, R.string.list_is_empty, Toast.LENGTH_SHORT).show();
        }
    }

    public void moveCamera(LatLng latLng, float zoom) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public void setSkateSpotList(List<SkateSpot> skateSpotList) {
        mapViewModel.setSkateSpotList(skateSpotList);
    }

    public void removeSkateSpotFromList(Long id) {
        Predicate<SkateSpot> isQualified = item -> item.getSkateSpotId().equals(id);
        List<SkateSpot> skateSpotList = mapViewModel.getSkateSpotList().getValue();
        if (skateSpotList != null) {
            boolean beenRemoved = skateSpotList.removeIf(isQualified);
            if (beenRemoved) {
                setSkateSpotList(skateSpotList);
            }
        }
    }

    public List<Address> getAddressList() {
        return fromLocation;
    }

    //CLUSTER
    private void setUpClustersSettings() {
        clusterManager = new ClusterManager<>(context, mMap);
        clusterManager.setOnClusterClickListener(this);
        clusterManager.setOnClusterItemClickListener(this);

        mMap.setOnCameraIdleListener(clusterManager);
    }

    @Override
    public boolean onClusterClick(Cluster<ClusterMarker> cluster) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterMarker item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        final LatLngBounds bounds = builder.build();

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, CLUSTER_MARGIN));
        return true;
    }

    @Override
    public boolean onClusterItemClick(ClusterMarker item) {
        mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(item.getPosition().latitude,
                item.getPosition().longitude)));

        if (!item.equals(mapViewModel.getSelectedClusterMarker().getValue())) {
            mapViewModel.setSelectedClusterMarker(item);
            return false;
        }
        mapViewModel.setSelectedClusterMarker(null);
        return false;

    }

    //DRAGGING
    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng latLng = marker.getPosition();
        try {
            fromLocation = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            Address address = fromLocation.get(0);
            String streetAddress = address.getAddressLine(0);
            marker.setTitle(streetAddress);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(CAMERA_POSITION, mMap.getCameraPosition());
    }
}
