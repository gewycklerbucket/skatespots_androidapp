package gewyckler.com.skatespots.repository;

import java.util.List;

import gewyckler.com.skatespots.model.SkateSpot;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface JsonPlaceHolderApi {

    @GET("skatespot/")
    Call<List<SkateSpot>> getSkateSpots();

    @GET("skatespot/{id}")
    Call<SkateSpot> getSkateSpotById(@Path("id") Long id);

    @GET("skatespot/city/{city}")
    Call<List<SkateSpot>> getSkateSpotCity(@Path("city") String city);

    @GET("skatespot/type/{skateSpotType}")
    Call<List<SkateSpot>> getSkateSpotByType(@Path("skateSpotType") String type);

    @POST("skatespot")
    Call<SkateSpot> createSkateSpot(@Body SkateSpot skateSpot);

    @DELETE("skatespot/{id}")
    Call<Long> deleteSkateSpot(@Path("id") Long id);

}
