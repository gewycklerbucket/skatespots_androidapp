package gewyckler.com.skatespots.fragments;

import static gewyckler.com.skatespots.utilities.Constants.MAP_FRAGMENT_TAG;

import android.content.Context;
import android.location.Address;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.List;

import gewyckler.com.skatespots.R;
import gewyckler.com.skatespots.interfaces.ILocalizationByMarkerListener;

public class LocalizationByMarkerFragment extends Fragment {
    private static final String TAG = "LocalizationByMarkerFRG";

    private ILocalizationByMarkerListener iLocalizationByMarkerListener;
    private View view;
    private MapFragment mapFragment;

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(TAG, "onAttach: called");
        super.onAttach(context);
        if (context instanceof ILocalizationByMarkerListener) {
            iLocalizationByMarkerListener = (ILocalizationByMarkerListener) context;
        } else {
            Log.e(TAG, context.toString() + " must implement ILocalizationByMarkerListener", new RuntimeException());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: called");
        view = inflater.inflate(R.layout.localization_by_marker_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: called");
        if (savedInstanceState != null) {
            mapFragment = (MapFragment) getParentFragmentManager().findFragmentByTag(MAP_FRAGMENT_TAG);
        } else {
            mapFragment = new MapFragment();
        }
        initMapFragment();
        viewInit();
    }

    private void viewInit() {
        Log.d(TAG, "viewInit: called");

        Button confirmBtn = view.findViewById(R.id.confirmation_button);
        confirmBtn.setOnClickListener(v -> {
            List<Address> addressList = mapFragment.getAddressList();

            if (addressList != null && !addressList.isEmpty()) {
                iLocalizationByMarkerListener.sentAddressList(addressList);
                Toast.makeText(getContext(), R.string.localizationAdded_info, Toast.LENGTH_SHORT).show();
                getParentFragmentManager().popBackStack();
            } else {
                Toast.makeText(getContext(), R.string.put_marker_to_confirm, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void initMapFragment() {
        Log.d(TAG, "initMapFragment: called");
        getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.localization_by_marker_frame_layout, mapFragment, MAP_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        iLocalizationByMarkerListener = null;
    }
}
