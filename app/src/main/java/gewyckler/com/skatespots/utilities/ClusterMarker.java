package gewyckler.com.skatespots.utilities;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import gewyckler.com.skatespots.model.SkateSpot;

public class ClusterMarker implements ClusterItem {
    private LatLng position;
    private String title;
    private String snippet;
    private SkateSpot skateSpot;


    public ClusterMarker(LatLng position, String title, String snippet, SkateSpot skateSpot) {
        this.position = position;
        this.title = title;
        this.snippet = snippet;
        this.skateSpot = skateSpot;
    }

    @NonNull
    @Override
    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    @Nullable
    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Nullable
    @Override
    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public SkateSpot getSkateSpot() {
        return skateSpot;
    }

    public void setSkateSpot(SkateSpot skateSpot) {
        this.skateSpot = skateSpot;
    }


}


