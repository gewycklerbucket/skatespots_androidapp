package gewyckler.com.skatespots.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import gewyckler.com.skatespots.R;

public class PhotoRecyclerViewAdapter extends RecyclerView.Adapter<PhotoRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "PhotoAdapter";
    private List<String> imageStringList = new ArrayList<>();

    public List<String> getSelectedImagesUri() {
        return imageStringList;
    }

    public void setImageStringList(List<String> uriList) {
        this.imageStringList = uriList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: called.");

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_photo_layout, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called.");
        //TODO: (DONE) usuwanie z listy, -> trzeba jakoś usunąć dany obrazek z SkateSpotSettingViewModel
        // (DONE) dodawanie,
        // domyslny obrazek zawsze na koncu ma byc???

        if (imageStringList.isEmpty()) {
            loadDefaultImage(holder);

        } else if (imageStringList.get(position) != null) {

            String s = imageStringList.get(position);
            byte[] bytes = Base64.decode(s, Base64.DEFAULT);

            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            holder.removeBtn.setVisibility(View.VISIBLE);
            loadBitmapByPicasso(holder.imageView.getContext(), bitmap, holder.imageView);
        }
    }

    private void loadBitmapByPicasso(Context pContext, Bitmap pBitmap, ImageView pImageView) {
        try {
            Uri uri = Uri.fromFile(File.createTempFile("temp_file_name", ".jpg", pContext.getCacheDir()));
            OutputStream outputStream = pContext.getContentResolver().openOutputStream(uri);
            pBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();
            Picasso.get().load(uri).into(pImageView);
        } catch (Exception e) {
            Log.e("LoadBitmapByPicasso", e.getMessage());
        }
    }

    private void loadDefaultImage(@NonNull ViewHolder holder) {
        holder.imageView.setImageResource(R.drawable.ic_base_photo);
        holder.removeBtn.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        if (imageStringList.isEmpty()) {
            return 1;
        }
        return imageStringList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        private final ImageButton removeBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.skatespot_photo);
            removeBtn = itemView.findViewById(R.id.remove_photo);
        }
    }
}
